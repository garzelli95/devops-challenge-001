#!/bin/bash

PROJECT='my_project_id'
REGION='us-central1'

TERRAFORM_SA='terraform-sa'
TFSTATE_BUCKET="bkt-tfstate-$PROJECT"
TERRAFORM_SA_KEY_FILE="$TERRAFORM_SA-key.json"

# create Terraform service account
gcloud iam service-accounts create $TERRAFORM_SA \
    --description='SA used by Terraform'

# add roles to Terraform SA
declare -a tf_sa_roles=(
    'roles/compute.admin'
    'roles/iam.serviceAccountUser'
    'roles/resourcemanager.projectIamAdmin'
    'roles/container.clusterAdmin'
    'roles/compute.viewer'
    'roles/compute.securityAdmin'
    'roles/container.developer'
    'roles/iam.serviceAccountAdmin'
    'roles/resourcemanager.projectIamAdmin'
)
for r in "${tf_sa_roles[@]}"
do
    gcloud projects add-iam-policy-binding $PROJECT \
        --member="serviceAccount:$TERRAFORM_SA@$PROJECT.iam.gserviceaccount.com" \
        --role=$r
done

# create bucket to store Terraform state
gsutil mb \
    -p $PROJECT \
    -c regional \
    -l $REGION \
    gs://$TFSTATE_BUCKET/

# grant permission on tfstate bucket to Terraform SA
gsutil iam ch serviceAccount:$TERRAFORM_SA@$PROJECT.iam.gserviceaccount.com:objectAdmin gs://$TFSTATE_BUCKET/

# create Terraform SA key
gcloud iam service-accounts keys create $TERRAFORM_SA_KEY_FILE \
    --iam-account="$TERRAFORM_SA@$PROJECT.iam.gserviceaccount.com"

# TODO enable bucket versioning
