resource "kubernetes_network_policy" "netpol_fe_allow_external_in" {
  metadata {
    name = "netpol-fe-allow-external-in"
  }

  spec {
    pod_selector {
      match_labels = {
        "io.kompose.service" = "kanban-ui"
      }
    }

    ingress {
      ports {
        port = "port-web"
      }
    }

    policy_types = ["Ingress"]
  }
}

resource "kubernetes_network_policy" "netpol_fe_allow_be_out" {
  metadata {
    name = "netpol-fe-allow-be-out"
  }

  spec {
    pod_selector {
      match_labels = {
        "io.kompose.service" = "kanban-ui"
      }
    }

    egress {
      to {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-app"
          }
        }
      }
    }

    policy_types = ["Egress"]
  }
}

resource "kubernetes_network_policy" "netpol_be_allow_fe_db_inout" {
  metadata {
    name = "netpol-be-allow-fe-db-inout"
  }

  spec {
    pod_selector {
      match_labels = {
        "io.kompose.service" = "kanban-app"
      }
    }

    ingress {
      from {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-ui"
          }
        }
      }

      from {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-data"
          }
        }
      }
    }

    egress {
      to {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-ui"
          }
        }
      }

      to {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-data"
          }
        }
      }
    }

    policy_types = ["Ingress", "Egress"]
  }
}

resource "kubernetes_network_policy" "netpol_db_allow_be_inout" {
  metadata {
    name = "netpol-db-allow-be-inout"
  }

  spec {
    pod_selector {
      match_labels = {
        "io.kompose.service" = "kanban-data"
      }
    }

    ingress {
      from {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-app"
          }
        }
      }
    }

    egress {
      to {
        pod_selector {
          match_labels = {
            "io.kompose.service" = "kanban-app"
          }
        }
      }
    }

    policy_types = ["Ingress", "Egress"]
  }
}

