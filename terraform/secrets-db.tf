resource "kubernetes_secret" "secret_db" {
  metadata {
    name = "secret-db"
  }

  data = {
    db       = "kanban"
    password = "kanban"
    user     = "kanban"
  }

  type = "Opaque"
}
