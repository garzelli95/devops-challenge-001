resource "kubernetes_config_map" "cm_db" {
  metadata {
    name = "cm-db"
  }

  data = {
    server = "kanban-postgres"
  }
}
