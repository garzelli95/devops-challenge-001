terraform {
  backend "gcs" {
    bucket      = "bkt-tfstate-prj-devops-challenge-001"
    credentials = "terraform-sa-key.json"
  }
}
