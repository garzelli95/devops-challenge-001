resource "kubernetes_persistent_volume_claim" "kanban_data" {
  metadata {
    name = "kanban-data"

    labels = {
      "io.kompose.service" = "kanban-data"
    }
  }

  spec {
    access_modes = ["ReadWriteOnce"]

    resources {
      requests = {
        storage = "100Mi"
      }
    }
  }
}

resource "kubernetes_deployment" "kanban_postgres" {
  metadata {
    name = "kanban-postgres"

    labels = {
      "io.kompose.service" = "kanban-postgres"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "io.kompose.service" = "kanban-postgres"
      }
    }

    template {
      metadata {
        labels = {
          "io.kompose.service" = "kanban-postgres"
        }
      }

      spec {
        volume {
          name = "kanban-data"

          persistent_volume_claim {
            claim_name = "kanban-data"
          }
        }

        container {
          name  = "kanban-postgres"
          image = "postgres:9.6-alpine"

          port {
            container_port = 5432
          }

          env {
            name = "POSTGRES_DB"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "db"
              }
            }
          }

          env {
            name = "POSTGRES_USER"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "user"
              }
            }
          }

          env {
            name = "POSTGRES_PASSWORD"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "password"
              }
            }
          }

          env {
            name  = "PGDATA"
            value = "/var/lib/postgresql/data/pgdata/"
          }

          volume_mount {
            name       = "kanban-data"
            mount_path = "/var/lib/postgresql/data"
          }
        }

        restart_policy = "Always"
      }
    }

    strategy {
      type = "Recreate"
    }
  }
}

resource "kubernetes_service" "kanban_postgres" {
  metadata {
    name = "kanban-postgres"

    labels = {
      "io.kompose.service" = "kanban-postgres"
    }
  }

  spec {
    port {
      name        = "port-db"
      port        = 5432
      target_port = "5432"
    }

    selector = {
      "io.kompose.service" = "kanban-postgres"
    }
  }
}

resource "kubernetes_deployment" "kanban_app" {
  metadata {
    name = "kanban-app"

    labels = {
      "io.kompose.service" = "kanban-app"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "io.kompose.service" = "kanban-app"
      }
    }

    template {
      metadata {
        labels = {
          "io.kompose.service" = "kanban-app"
        }
      }

      spec {
        container {
          name  = "kanban-app"
          image = "garzelli95/devops-challenge-app:1.0.2"

          port {
            container_port = 8080
          }

          env {
            name = "DB_SERVER"
            value_from {
              config_map_key_ref {
                name = "cm-db"
                key  = "server"
              }
            }
          }

          env {
            name = "POSTGRES_DB"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "db"
              }
            }
          }

          env {
            name = "POSTGRES_USER"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "user"
              }
            }
          }

          env {
            name = "POSTGRES_PASSWORD"
            value_from {
              secret_key_ref {
                name = "secret-db"
                key  = "password"
              }
            }
          }

        }

        restart_policy = "Always"
      }
    }
  }
}

resource "kubernetes_service" "kanban_app" {
  metadata {
    name = "kanban-app"

    labels = {
      "io.kompose.service" = "kanban-app"
    }
  }

  spec {
    port {
      name        = "port-app"
      port        = 8080
      target_port = "8080"
    }

    selector = {
      "io.kompose.service" = "kanban-app"
    }
  }
}

resource "kubernetes_deployment" "kanban_ui" {
  metadata {
    name = "kanban-ui"

    labels = {
      "io.kompose.service" = "kanban-ui"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "io.kompose.service" = "kanban-ui"
      }
    }

    template {
      metadata {
        labels = {
          "io.kompose.service" = "kanban-ui"
        }
      }

      spec {
        container {
          name  = "kanban-ui"
          image = "garzelli95/devops-challenge-web:1.0.0"

          port {
            container_port = 80
          }
        }

        restart_policy = "Always"
      }
    }
  }
}

resource "kubernetes_service" "kanban_ui" {
  metadata {
    name = "kanban-ui"

    labels = {
      "io.kompose.service" = "kanban-ui"
    }
  }

  spec {
    port {
      name        = "port-web"
      protocol    = "TCP"
      port        = 80
      target_port = "80"
    }

    selector = {
      "io.kompose.service" = "kanban-ui"
    }

    type = "LoadBalancer"
  }
}

