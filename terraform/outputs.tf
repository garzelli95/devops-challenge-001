output "region" {
  value       = var.region
  description = "Google Cloud region"
}

output "project_id" {
  value       = var.project_id
  description = "GCP project id"
}

output "cluster_name" {
  value       = module.gke.name
  description = "GKE cluster name"
}

