# Kanban Boards on Kubernetes

## Kanban Application

The application is a simple Kanban board, whose code has taken from
https://github.com/wkrzywiec/kanban-board. The application is made up of:

- frontend (TypeScript, Angular)
- backend (Java, Spring)
- database (PostgreSQL)

I fixed just a CORS configuration in the backend.

I chose this application for its simplicity.

## Running the Application Locally

### Docker Compose

To run the application locally:

```bash
cd kanban-board
docker compose up -d
```

Then, visit http://localhost:4200/ on your browser.

To stop the application run `docker compose down`.

To run just some services:

```bash
docker compose up -d <service1> <service2>
# e.g.
docker compose up -d db-kanban web-kanban
```

To view logs run one of the following commands. You can omit `-f`.

```bash
docker logs -f kanban-ui
docker logs -f kanban-app
docker logs -f kanban-postgres
```

Attach a Bash shell with (for example) `docker exec -it kanban-postgres bash`.

### Minikube

```bash
minikube start --ports=127.0.0.1:30080:30080
eval $(minikube docker-env)

kubectl apply -f k8s-manifests
kubectl get pods -w

# visit http://localhost:30080/

minikube delete
```

## Kubernetes Manifests

Deployments, services, and other objects are defined in the YAML files found in
the "k8s-manifests" folder. They were generated with kompose from a
docker-compose file, with slight modifications. The most important difference is
the type of the frontend Service, which was changed to LoadBalancer to make it
available to the external.

Secrets should not be put under version control, here database credentials are
just because it is easier to apply them.

## Deployment to GCP

To handle different environments, I would probably go for a cluster with two
namespaces for development and QA and a separate one for production.

To handle the differences between these environments, I would try with Helm or
Kustomize, but I have never used them before.

The application can be deployed to GCP with Terraform, but a bit of setup is
required. Use "gcp-setup.sh" to:

1. create a service account for Terraform with the required permissions
2. create a Cloud Storage bucket as Terraform state backend
3. obtain the service account key for authentication (even if impersonation is
   preferable)

The Terraform configuration creates both the GKE cluster and Kubernetes objects,
i.e., it also deploys the application. It would be better to keep these two
thing separate. To apply it, you need to set variables and have a service
account key. Add a terraform.tfvars file with a content similar to the
following:

```
credentials        = "terraform-sa-key.json"
project_id         = <project id>
region             = "us-central1"
zones              = ["us-central1-a", "us-central1-b", "us-central1-f"]
name               = "gke-cluster-kanban"
service_account    = "terraform-sa@<project id>.iam.gserviceaccount.com"
```

Now you can run:

```bash
terraform init
terraform apply
```

Wait for some minutes and the cluster and the application will be up and
running.

To view the application:

```bash
gcloud container clusters get-credentials $(terraform output -raw cluster_name) --region $(terraform output -raw region)
kubectl get svc # then visit the external IP of the LoadBalancer service
```